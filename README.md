# Crafty 3 to 4 Migrator

Tool for migrating from Crafty 3 to Crafty 4. Note that this utility is not designed for Docker-based implementations; those must currently be migrated manually.

# Getting started

## Migrating from 3.x

### Windows

1. Install Crafty 4.0 according to the install guides located [here](https://wiki.craftycontrol.com/en/4/Windows%20Setup%20Documentation)
2. Download the migration script from [here](https://gitlab.com/crafty-controller/crafty-3-to-4-migrator/-/raw/main/run_migration.py?inline=false)
3. Run the migration script
    1. Open Command Prompt, then navigate to the path where you downloaded the migration script using `cd C:\<PATH TO SCRIPT HERE>`
    2. Run the migration script using `python run_migration.py`
    3. Input the path to the **old installation of Crafty 3** when prompted (e.g. `C:\Users\Minecraft\Crafty`)
    4. The migration script will create a folder called `Migrate` in the path where your script is located - note this path down, it's a surprise tool that will help us later
4. Run your Crafty installation for the first time, then, once the command prompt appears, run `import3`
5. When prompted to input the path to the Migrations folder, enter the path noted in step 3.4
6. Let Crafty run its course, then it will have all of your users and servers imported!

### Linux/other non-windows installations

1. Install Crafty 4.0 according to your relevant migration guide on our [wiki](https://wiki.craftycontrol.com)
2. Download the migration script by running `curl https://gitlab.com/crafty-controller/crafty-3-to-4-migrator/-/raw/main/run_migration.py?inline=false -o run_migration.py`
3. Run the migration script
    1. In the path where you downloaded the script, run `python run_migration.py`
    2. Input the path to the **old installation of Crafty 3** when prompted (e.g. `/var/opt/minecraft/crafty/crafty-web`)
    3. The migration script will create a folder called `Migrate` in the path where your script is located - note this path down, it's a surprise tool that will help us later
4. Run your Crafty installation for the first time, then, once the command prompt appears, run `import3`
5. Whe prompted to input the path to the Migrations folder, enter the path noted in step 3.3
6. Let Crafty run its course, then it will have all of your users and servers imported!
