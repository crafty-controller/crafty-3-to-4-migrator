import os
import json
import sqlite3

# This method will allow easier 3.x to 4.x migration for the Databases
def migrate_to_json():
    
    # Get the path from the user
    user_input = input('Input the main directory of your old crafty install (Where crafty.py is located): ')
    
    # Check if the path given by the user actually exists
    if not os.path.exists(os.path.join(user_input)):
        print('That path is non-existant. Please ensure you have put the correct path')
        exit(0)
    
    # Check if the crafty.sqlite file actually exists
    if not os.path.exists(os.path.join(user_input, 'app', 'config', 'crafty.sqlite')):
        print('Could not find "crafty.sqlite" database. Has it been moved?')
        exit(0)
    
    # Set our database variable
    database = sqlite3.connect(os.path.join(user_input, 'app', 'config', 'crafty.sqlite'))
    
    # Start off checking if a migrate directory exists
    if not os.path.exists(os.path.join(os.getcwd(), 'migrate')):
        os.mkdir(os.path.join(os.getcwd(), 'migrate'))
        
    # Now we will begin
    with database:
        # Get all the table names in the sql database
        cursor = database.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        table_names = [table[0] for table in cursor.fetchall()]

        # Loop through all the tables in the database
        for table in table_names:
            print(f"Migrating {table}...")
            dictionaries = []

            # Select everything from that table
            query = cursor.execute(f"SELECT * FROM {table}")

            # Get all the column names
            names = [description[0] for description in query.description]
            
            # Iterate through all the rows and save the col names matched with it's value at the end
            for row in query:
                entry = {k: v for k, v in zip(names, row)}
                dictionaries.append(entry)
                
            # Save the newly made Dictionary to a JSON file named after the table
            with open(f'migrate/{table}.json', 'w+') as f:
                if dictionaries.__len__() > 1:
                    json.dump(dictionaries, f, indent=4)
                else:
                    try:
                        json.dump(dictionaries[0], f, indent=4)
                    except:
                        # Probably an empty table, just make it an empty dictionary
                        json.dump({}, f, indent=4)

migrate_to_json()